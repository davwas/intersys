﻿using System;

namespace CommonDigit
{
    class Test
    {
        public static void CommonDigit()
        {
            int arrLength = Int32.Parse(Console.ReadLine());
            string[] array = Console.ReadLine().Split(" ");
            if (arrLength < 2 || arrLength > 20 || array.Length != arrLength)
            {
                throw new Exception("Wrong input");
            }

            string joinedString = String.Join("", array);
            int[] asciiCharactersCount = new int[256];
            Array.Clear(asciiCharactersCount, 0, 256);
            int biggestCharacterCount = 0;
            char mostCommonCharacter = char.MinValue;
            foreach (var character in joinedString)
            {
                asciiCharactersCount[character]++;
                if (asciiCharactersCount[character] >= biggestCharacterCount)
                {
                    biggestCharacterCount = asciiCharactersCount[character];
                    mostCommonCharacter = character;
                }
            }
            Console.WriteLine(mostCommonCharacter);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Test.CommonDigit();
        }
    }
}
