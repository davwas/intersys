function optionalPow(a, b){
  if (window.confirm(`Press OK to compute ${a}^${b} or Cancel to compute ${b}^${a}`)) {
    return Math.pow(a,b);
  } else {
    return Math.pow(b,a);
  }
}