﻿using System;
using System.Linq;

namespace Permutation
{
    class Test
    {
        public static void Permutation()
        {
            int[] array1 = Array.ConvertAll(Console.ReadLine().Split(" "), Convert.ToInt32);
            int[] array2 = Array.ConvertAll(Console.ReadLine().Split(" "), Convert.ToInt32);
            if (array1.Length == 11 && array1.Length == array2.Length)
            {
                Array.Sort(array1);
                Array.Sort(array2);
                if (array1.SequenceEqual(array2)) Console.WriteLine("YES");
                else Console.WriteLine("NO");

            }
            else Console.WriteLine("Wrong input.");
        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            Test.Permutation();
        }
    }
}
