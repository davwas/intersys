﻿using System;

namespace DigitSum
{
    class Test
    {
        public static void GetNumberWithBiggestDigitSum()
        {
            int arrLength = Int32.Parse(Console.ReadLine());
            int[] array = Array.ConvertAll(Console.ReadLine().Split(" "), Convert.ToInt32);
            if (array.Length != arrLength)
            {
                throw new Exception("Wrong input");
            }
            int biggestDigitSum = 0, numberWithBiggestDigitSum = 0;
            foreach (var number in array)
            {
                int digitSum = DigitSum(number);
                if (digitSum > biggestDigitSum)
                {
                    biggestDigitSum = digitSum;
                    numberWithBiggestDigitSum = number;
                }
                else if (digitSum == biggestDigitSum)
                {
                    if (number > numberWithBiggestDigitSum) biggestDigitSum = number;
                }
            }
            Console.WriteLine(Array.LastIndexOf(array, numberWithBiggestDigitSum));
        }
        private static int DigitSum(int value)
        {
            int digitSum = 0, tmp;
            while (value > 0)
            {
                tmp = value % 10;
                digitSum += tmp;
                value = value / 10;
            }
            return digitSum;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Test.GetNumberWithBiggestDigitSum();
        }
    }
}
