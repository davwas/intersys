function fixage(x){
  var txt='';
  for (var i = 0; i < x.length; i++) {
    if (x[i] >= 18 && x[i] <= 60) {
      if (i === x.length-1) {
          txt += x[i];
      } else {
          txt += x[i] + ',';
      }
    }
  }
  if (txt === '') {
    txt = 'NA';
  }
  return txt;
}