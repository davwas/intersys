﻿using System;
using System.Collections.Generic;

namespace FindingPrimes
{
    class Test
    {
        public static void FindPrimes()
        {
            int testCases = Int32.Parse(Console.ReadLine());
            int i = 1;
            List<int[]> casesList = new List<int[]>();
            while (i <= testCases)
            {
                int[] tmp = Array.ConvertAll(Console.ReadLine().Split(" "), Convert.ToInt32);
                if ((tmp[0] >= 1 && tmp[1] >= 1 && tmp[0] <= tmp[1]))
                {
                    casesList.Add(tmp);
                }
                else
                {
                    Console.WriteLine("Wrong input.");
                    break;
                }
                i++;
            }
            List<int> numberOfPrimesInTestCases = new List<int>();
            foreach (var _case in casesList)
            {
                GetNumberOfPrimesWithinInterval(ref numberOfPrimesInTestCases, _case[0], _case[1]);
            }
            PrintList(numberOfPrimesInTestCases);
        }
        private static void GetNumberOfPrimesWithinInterval(ref List<int> numberOfPrimesInTestCases, int start, int end)
        { 
            int primes = 0;
            if (start == 1) start++;
            while (start <= end)
            {
                bool isPrime = true;
                for (int i = 2; i <= Math.Sqrt(start); i++)
                {
                    if (start % i == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime) primes++;
                start++;
            }
            numberOfPrimesInTestCases.Add(primes);
        }
        private static void PrintList(List<int> list)
        {
            foreach (var number in list)
            {
                Console.WriteLine(number);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Test.FindPrimes();
        }
    }
}
