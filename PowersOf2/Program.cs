﻿using System;
using System.Collections.Generic;

namespace PowersOf2
{
    class Test
    {
        public static void PowersOf2()
        {
            string input = Console.ReadLine();
            List<int> inputList = new List<int>();
            while (input != "")
            {
                inputList.Add(Int32.Parse(input));
                input = Console.ReadLine();
            }
            inputList.Sort();
            List<int> powers = new List<int>();
            foreach (var number in inputList)
            {
                int i = 1;
                while (i <= number)
                {
                    int tmp = i & number;
                    if (tmp != 0 && !powers.Contains(tmp))
                    {
                        powers.Add(tmp);
                    }
                    i <<= 1;
                }
            }
            powers.Sort();
            PrintList(powers);
        }
        private static void PrintList(List<int> list)
        {
            if (list.Count == 0)
            {
                Console.WriteLine("NA");
            }
            else
            {
                for (int i = 0; i < list.Count - 1; i++)
                {
                    Console.Write(list[i] + ", ");
                }
                Console.Write(list[list.Count - 1]);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Test.PowersOf2();
        }
    }
}
