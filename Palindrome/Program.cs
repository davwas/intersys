﻿using System;
using System.Collections.Generic;

namespace Palindrome
{
    class Test
    {
        public void Palindrome()
        {
            string input = Console.ReadLine().ToLower();
            List<char> charList = new List<char>();
            foreach (var character in input)
            {
                int characterValue = Convert.ToInt32(character);
                if (characterValue >= 97 && characterValue <= 122)
                {
                    charList.Add(character);
                }
            }
            bool truePalindrome = isTruePalindrome(charList);
            string result = truePalindrome ? "YES" : "NO";
            Console.WriteLine(result);
        }

        private bool isTruePalindrome(List<char> list)
        {
            bool truePalindrome = true;
            int n = list.Count - 1;
            for (int i = 0; i < list.Count / 2; i++)
            {
                if (list[i] != list[n--])
                {
                    truePalindrome = false;
                    break;
                }
            }
            return truePalindrome;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Test t = new Test();
            t.Palindrome();
        }
    }
}
