﻿using System;
using System.Linq;

namespace ReverseArray
{
    class Test
    {
        public static void ReverseArray()
        {
            int arrLength = Int32.Parse(Console.ReadLine());
            int[] array = Array.ConvertAll(Console.ReadLine().Split(" "), Convert.ToInt32);
            if (arrLength == array.Length)
            {
                Array.Reverse(array);
                for (int i = 0; i < array.Length - 1; i++)
                {
                    Console.Write(array[i] + " ");
                }
                Console.Write(array[array.Length - 1]);
            }
            else Console.WriteLine("Wrong input.");
        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            Test.ReverseArray();
        }
    }
}
